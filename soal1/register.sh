#!usr/bin/bash
# Checks whether the necessary files are there.
if [ ! -d log.txt ]; then
    touch log.txt
fi

if [ ! -d ./users ]; then
    mkdir ./users
fi

if [ ! -d ./users/user.txt ]; then
    touch ./users/user.txt
fi

echo "Please register your username and password"

dateTemp="$(date +%m/%d/%Y\ %T)"

echo "Username: "
read username

#Will throw an error message if same username is found.
if grep -q "$username" users/user.txt; then
    echo -e "$dateTemp REGISTER: ERROR User already exists" >>./log.txt
    echo "Username already registered."
    exit
fi

echo "Password: "
read -s password

isWrong=0
if [ ${#password} -lt 8 ]; then
    echo "-Password is less than 8 characters."
    isWrong=1
fi
if [[ $password = ${password,,} ]]; then
    echo "-Password has no uppercase."
    isWrong=1
fi
if [[ $password = ${password^^} ]]; then
    echo "-Password has no lowercase."
    isWrong=1
fi
if [[ ! $password =~ [0-9]+ ]]; then
    echo "-Password is not alphanumeric."
    isWrong=1
fi
if [ $password == $username ]; then
    echo "-Password is equal to username."
    isWrong=1
fi
if [ $isWrong == 1 ]; then
    exit
fi

echo -e "$dateTemp REGISTER: INFO User $username registered successfully" >>./log.txt
echo "$username?$password" >>users/user.txt
echo "User registered!"
