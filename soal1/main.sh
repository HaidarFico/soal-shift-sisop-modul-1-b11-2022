#!/usr/bin/bash
loginChecker() {
    # Will check for the username and password from the user.txt file.
    # Will return 0 if successful, 1 if unsuccessful.
    if grep -q "$1?$2" ./users/user.txt; then
        return 0
    else
        return 1
    fi
}

att() {
    # Will check for the sTime and usTime variable from grep, then will echo to the console.
    username=$1
    sTime=$(grep -c "LOGIN: INFO User $username logged in" log.txt)
    usTime=$(grep -c "LOGIN: ERROR Failed Login attempt on user $username" log.txt)
    echo "The user $1 has logon successfully $sTime and unsuccessfully $usTime times"
}

dl() {
    # Handles the dl argument.
    url="https://loremflickr.com/320/240"
    dateTemp="$(date +%Y-%m-%d)"

    # Will trigger if there is already an existing zip file
    if [ -f $dateTemp\_$username.zip ]; then
        unzip -P $password $dateTemp\_$username.zip
        latestNum="$(ls $dateTemp\_$username | wc -l)"
        for ((num = latestNum + 1; num <= latestNum + $1; num = num + 1)); do
            if [ $num -lt 10 ]; then
                wget -O $dateTemp\_$username/PIC_0$num ${url}
            else
                wget -O $dateTemp\_$username/PIC_$num ${url}
            fi
        done
        zip -P $password $dateTemp\_$username.zip $dateTemp\_$username/*
        rm -r $dateTemp\_$username
    else
        mkdir $dateTemp\_$username
        for ((num = 1; num <= $1; num = num + 1)); do
            if [ $num -lt 10 ]; then
                wget -O $dateTemp\_$username/PIC_0$num ${url}
            else
                wget -O $dateTemp\_$username/PIC_$num ${url}
            fi
        done
        zip -P $password $dateTemp\_$username.zip $dateTemp\_$username/*
        rm -r $dateTemp\_$username
    fi
}

# Checks whether the necessary files are there.
if [ ! -d log.txt ]; then
    touch log.txt
fi

if [ ! -d users ]; then
    mkdir users
fi

if [ ! -d users/user.txt ]; then
    touch users/user.txt
fi

echo "Please enter your username"
read username
echo "Please enter your password"
read -s password
dateTemp="$(date +%m/%d/%Y\ %T)"
loginChecker $username $password
if [ $? -eq 1 ]; then
    echo "Password was incorrect"
    echo -e "$dateTemp LOGIN: ERROR Failed Login attempt on user $username" >>./log.txt
    exit
else
    echo -e "$dateTemp LOGIN: INFO User $username logged in" >>./log.txt
    echo "Input was correct. Entering program..."
fi
# Will run in a loop until user exits from program.
while :; do
    echo "Please enter a command:"
    read choice1 choice2
    if [ $choice1 == "att" ]; then
        att $username
    elif [ $choice1 == "dl" ]; then
        dl $choice2
    else
        echo "Please enter a valid command"
    fi
done
