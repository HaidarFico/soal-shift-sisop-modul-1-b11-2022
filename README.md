Soal Shift Sisop Modul 1:
Kelompok B11:
Haidar Fico Ramadhan Aryputra - 5025201185
Nethaneel Patricio Linggar - 5025201180

Soal pertama:
Untuk soal pertama, kami membagi masalah ini atas beberapa elemen, yaitu register, login, dl, dan att.
Bagian registrasi terdapat pada file register.sh.

![](Photos/registerdotsh.png)

File register.sh ini memiliki fungsi utama untuk menerima input
data dari user berupa username dan password. Hal pertama yang dilakukan oleh script ini adalah melakukan
if check untuk memverifikasi file-file yang dibutuhkan. Dalam program ini, file-file yang dibutuhkan adalah
log.txt dan folder user yang berisi file users.txt. Jika terdapat file yang belum ada, maka program akan
membuat file tersebut. Setelah itu, program akan meminta input username dan password oleh user. Input password
tidak akan bisa terlihat oleh user karena option -s pada command read. Setelah menerima input username dan password,
program akan memverifikasi bahwa input tersebut sesuai dengan ketentuan yang sudah diberikan. Untuk username, akan
diverifikasi bahwa username yang dimasukkan belum pernah dimasukkan sebelumnya. Hal ini dilakukan dengan melakukan
grep terhadap file users/user.txt. Jika grep menemukan username yang sama, maka akan diberikan error message dan akan
exit dari program. Setelah itu, password yang dimasukkan akan diuji atas beberapa faktor. Pertama, password tidak
boleh lebih dari 8 character. Kedua, password harus memiliki minimal satu uppercase dan satu lowercase. Ketiga, password harus bersifat alphanumeric. Keempat, password tidak boleh sama dengan username. Setelah pengujian berhasil, akan dilakukan
write kepada log.txt dan username password akan di write di users/user.txt.

Bagian kedua dari soal ini adalah main.sh. File ini dapat dibagi atas beberapa elemen, yaitu login, dl, dan att. Bagian
login dari elemen ini akan mencari username dan password dalam file users/user.txt. Jika input yang diberikan oleh user
dapat ditemukan oleh users/user.txt, maka program dapat berlanjut setelah menulis ke log.txt. Akan tetapi, jika terdapat
kesalahan dalam penginputan username dan password maka akan diberi error message, menulis ke log.txt dan mengeluarkan diri
dari program. Setelah melakukan login, maka user akan diberi kesempatan untuk menginput antara att atau dl. Kedua command
ini memiliki dua fungsi yang berbeda.

![](Photos/maindotsh.png)

Fungsi att akan memberikan user output yang berupa perhitungan beberapa kali user tersebut telah melakukan login terhadap sistem.
Counter tersebut akan dibagi menjadi dua yaitu login yang berhasil dan yang tidak berhasil. Counter tersebut akan didapat dengan
melakukan grep terhadap file log.txt. Karena login successful dan unsuccessful-nya dipisah dengan jelas. Maka cukup mudah untuk
melakukan grep yang terpisah atas dua login tersebut.

Fungsi dl akan mengunduh file dari url yang telah disediakan oleh soal sebanyak N kali. Jumlah file yang diunduh akan sesuai dengan
argumen yang diberikan oleh user. Akan dibuat sebuah folder yang dinamakan sesuai tanggal dan nama user. File-file tersebut akan
ditempatkan pada folder tersebut dan dinamakan berurutan dari 01. Setelah ditempatkan akan di-zip dengan password. Jika pada saat 
fungsi baru dilaksanakan terdeteksi bahwa terdapat sebuah zip dengan nama yang sama, maka zip akan di-unzip lalu file-file baru
akan diunduh dengan urutan yang sesuai dengan urutan di folder lama tersebut. Hal ini didapatkan dengan menghitung jumlah file
folder yang lama.

Soal Kedua:

Untuk soal kedua, kami membagi soal tersebut atas lima bagian. Bagian-bagian
tersebut dapat terlihat dalam 
kode yaitu ratarataWriter, mostConnectWriter, curltotalWriter,
IPListWriter, dan main function.
Main function merupakan function pertama yang dijalankan. Fungsi utama dari function
ini adalah untuk menjalankan function-function yang lain.

![](Photos/soal2_forensic_daposdotsh.png)

Untuk bagian ratarataWriter. Function ini memiliki fungsi utama yaitu untuk menghitung
data yang berupa rata-rata koneksi per jam. Data ini didapat dengan pertama
menggunakan awk untuk print seluruh data, lalu sort untuk mengurutkan data, lalu
uniq -c untuk menghitung total data dari tiap jam. Terakhir akan ada awk untuk mengeluarkan
rata-rata koneksi per jam. Di awk terakhir juga menghitung total jamnya dengan 
menghitung line numbernya.

Untuk bagian mostConnectWriter akan mencari dan menulis data yaitu IP address dan
beberapa kalinya user yang paling sering melakukan koneksi. Hal ini dilakukan
dengan menggunakan awk yang akan menulis IP address tiap log, lalu sort dan uniq -c
akan menghitung beberapa kali mereka telah melakukan koneksi tiap IP address dan
sort -rn akan menampilkan IP yang pertama. Lalu awk akan mengambil yang paling pertama
dan menulis ke file.

Function curlWriter menggunakan awk untuk mencari total log yang merupakan percobaan
curl. Lalu akan disimpan dalam curlTotal. Lalu akan ditulis ke file.

Function yang terakhir berupa IPListWriter. Fungsi ini pertama memakai awk untuk
menulis IP dan tanggal. Setelah itu, akan menggunakan sort untuk mensortirnya lalu
awk akan menghilangkan tanda kutif dari output tersebut. Terakhir akan ada awk
untuk memfilter agar hanya mendapat IP address dari tanggal dan waktu tertentu.
Terakhir uniq akan menghilangkan IP address yang sama dan awk terakhir akan memberi 
hasil ke file.

Dari fungsi-fungsi ini akan mendapat output sebagai berikut:
![](Photos/resultdottxt.png)

![](Photos/rataratadottxt.png)

Soal Ketiga:
Untuk soal ketiga, kita membuat dua script, minute_log.sh dan aggregate_minutes_to_hourly_log.sh.
File minute_log gunanya memonitor memory dan size dari directory selama tiap menit, sementara file aggregate_minutes_to_hourly_log gunanya merangkum semua minute_log selama satu jam sebelumnya.

minute_log:
![](Photos/minute_log.png)
Pada bagian pertama, ada variabel nameTemp yang mencari nama dari user yang menjalankan script, sehingga tidak perlu menyesuaikan nama user jika script dijalankan pada komputer yang berbeda.
Lalu program melihat apakah ada folder log pada /home/{user}. Jika tidak ada, maka folder log akan dibuat.
Variabel directory dibuat untuk membuat nama file .log yang sesuai dengan format tanggal saat script dijalankan.
Program pun mengeluarkan hasil dari monitoring memory dan swap memory, beserta size dari path /home/{user}.
Setelah monitoring dijalankan, hasil akan diprint ke dalam directory yang sudah ditentukan.

CronJob minute_log:
Supaya minute_log dijalankan tiap menit, maka dilakukan cronjob. Caranya dengan menjalankan crontab -e ke dalam terminal dan memasukkan line berikut ke dalam text editornya.
* * * * * /home/{user}/soal-shift-sisop-modul-1-b11-2022./soal3/minute_log.sh

aggregate_minutes_to_hourly_log:
![](Photos/aggregate_minutes_to_hourly_logdotsh.png)
Pada bagian ini, pertama diambil variabel hour untuk menyaring log sehingga hanya mendapat yang
sejam yang lalu. Setelah itu, semua nama file log yang dipakai akan ditaruh di dalam array yang bernama
files_arr.  Dari files_arr tersebut akan dipindai. Setiap column akan ditaruh di dalam temporary file
sementara lalu akan di sort satu persatu. Setelah itu, nilai minimum dan maksimum tiap column akan
ditaruh dalam array minArr dan maxArr. Setelah itu, akan di tulis ke file dan diberi chmod 700.

CronJob aggregate_minutes_to_hourly_log:
Sama seperti minute_log, supaya dijalankan tiap jam maka akan dilakukan cronjob. Berikut line yang akan dimasukkan ke dalam text editor.
0 * * * * /home/{user}/soal-shift-sisop-modul-1-b11-2022./soal3/aggregate_minutes_to_hourly_log.sh