#!bin/bash
ratarataWriter() {
    # Bagian pertama akan memprint semua yang di bagian ketiga (jam), habis itu akan ngesort sesuai jam
    # uniq -c akan ngitung sesuai jam, lalu awk terakhir akan menghitung rata-ratanya dan menulis
    # Message ke file. 
    awk -F: 'NR>1{print $3}'  log_website_daffainfo.log | sort | uniq -c | awk '{sum+=$1} END {printf "Rata-rata serangan adalah sebanyak %d requests per jam\n", sum/(NR-1)}' > forensic_log_website_daffainfo_log/ratarata.txt
}

mostConnectWriter() {
    # Bagian pertama akan Memprint IP address lalu sort akan mensortir, uniq -c akan menghitung
    # IP addressnya, lalu sort -rn akan memsort IP yang pertama, terakhir awk akan mengambil
    # yang paling pertama dan menulis ke file.
    awk -F: 'NR>1{print $1}' log_website_daffainfo.log | sort | uniq -c | sort -rn | awk 'NR==1{gsub(/"/,"")}1' | awk 'NR==1{printf "IP yang paling banyak mengakses server adalah:  %s sebanyak %s requests\n\n", $2, $1}' > forensic_log_website_daffainfo_log/result.txt 
}

curlTotalWriter() {
    # Bagian pertama akan memprint semua line yang memiliki curl, lalu akan ada awk untuk menghitung total line dan menulis ke file.
    awk -F: '/curl/{print}' log_website_daffainfo.log | awk 'END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", NR}' >> forensic_log_website_daffainfo_log/result.txt 
}

IPListWriter() {
    # awk akan menulis semua IP address dan tanggal, lalu akan di sort lalu awk akan menghilangkan tanda kutip,
    # setelah itu awk akan memfilter log dengan jadwal tertentu lalu uniq akan menghilangkan line yang sama, terakhir
    # awk akan menulis ke file.
    awk -F':"' 'NR>1{printf "%s %s\n", $1, $2}' log_website_daffainfo.log | sort | awk '{gsub(/"/,"")}1' | awk '/22\/Jan\/2022:02:/ {print $1}' | uniq | awk '{print}' >> forensic_log_website_daffainfo_log/result.txt
}

if [ -d "./forensic_log_website_daffainfo_log" ]; then
    echo "Previously existing folder found, deleting..."
    rm -r ./forensic_log_website_daffainfo_log
fi
awk 'BEGIN {system("mkdir forensic_log_website_daffainfo_log && touch forensic_log_website_daffainfo_log/ratarata.txt && touch forensic_log_website_daffainfo_log/result.txt")}'
ratarataWriter
mostConnectWriter
curlTotalWriter
IPListWriter
echo "Program finished."
