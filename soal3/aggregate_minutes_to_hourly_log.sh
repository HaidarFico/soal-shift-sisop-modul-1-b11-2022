#!/bin/bash

hour=$(date +%H)
hour=$(($hour - 1))
DateNow=$(date +"%Y%m%d%H")
files_arr=($(ls -l ~/log | awk -v var="$hour" '{if(int(substr($9, 17, 2))==var) print($9)}'))
touch temp.txt
touch result.txt
declare -a minArr
declare -a maxArr
declare -a averageArr

for file in "${files_arr[@]}"; do
    awk 'NR==2{printf "%s,0\n", $1}' ~/log/$file >>temp.txt
done
 
for ((num=1;num<12;num=num+1)); do
    awk -F "," "{print \$$num}" temp.txt > tempColumn$num
    cat tempColumn$num | sort -n > tempSorted$num
    rm tempColumn$num
    minArr[$num]=$(awk 'NR==1{print}' tempSorted$num)
    maxArr[$num]=$(awk 'END{print}' tempSorted$num)
    if [ $num = 10 ]; then
        averageArr[$num]=$(awk 'NR==1{print $1}' tempSorted$num)
    else
        averageArr[$num]=$(awk '{sum+=int($1)} END {printf "%.1f\n",sum/NR}' tempSorted$num)
    fi
    rm tempSorted$num
done

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >result.txt
echo -n "minimum," >> result.txt
for ((num=1;num<12;num=num+1)); do
    echo -n "${minArr[$num]}," >> result.txt
done
echo " " >> result.txt
echo -n "maximum," >> result.txt
for ((num=1;num<12;num=num+1)); do
    echo -n "${maxArr[$num]}," >> result.txt
done
echo " " >> result.txt
echo -n "average," >> result.txt
for ((num=1;num<12;num=num+1)); do
        echo -n "${averageArr[$num]}," >> result.txt
done

rm temp.txt

mv result.txt ~/log/matrix_agg_$DateNow.log
chmod 700 ~/log/matrix_agg_$DateNow.log
