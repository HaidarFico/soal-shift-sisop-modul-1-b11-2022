#!/bin/bash
# nameTemp digunakan untuk mencari path yang diperlukan.
nameTemp=$(whoami)

# Jika tidak ada folder log di home, maka akan dibuat.
if [ ! -d /home/$nameTemp/log ]
then
    mkdir /home/$nameTemp/log
fi

directory="/home/$nameTemp/log/metrics_$(date +"%Y%m%d%H%M%S").log"
# Format dari data yang akan diberi
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $directory

# Memperlihatkan memory pada RAM.
MEM="$(free | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
# Memperlihatkan swap memory pada RAM.
SWAP="$(free | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')"
# Memperlihatkan size dari path tersebut.
STORAGE="$(du -sh /home/$nameTemp/ | awk '{printf "%s,%s", $2,$1}')"

echo "$MEM,$SWAP,$STORAGE" >> $directory
